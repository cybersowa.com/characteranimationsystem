﻿using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

public class AnimMixer : MonoBehaviour
{
    [Range(0.0f, 1.0f)]
    public float weight;
    public AnimationClip idleClip;
    public AnimationClip romClip;
    public AvatarMask mask;

    PlayableGraph m_Graph;
    // private AnimationMixerPlayable m_MixerPlayable;
    private AnimationLayerMixerPlayable m_AvatarMixer;
    
    void OnEnable()
    {
        // Load animation clips.
        if (idleClip == null || romClip == null)
            return;

        var animator = GetComponent<Animator>();
        
        // Create graph with custom mixer.
        m_Graph = PlayableGraph.Create("AnimMixer");
        m_Graph.SetTimeUpdateMode(DirectorUpdateMode.GameTime);
        m_AvatarMixer = AnimationLayerMixerPlayable.Create(m_Graph, 2);
        m_AvatarMixer.ConnectInput(0, AnimationClipPlayable.Create(m_Graph, idleClip), 0, 1.0f);
        m_AvatarMixer.ConnectInput(1, AnimationClipPlayable.Create(m_Graph, romClip), 0, weight);
        m_AvatarMixer.SetLayerMaskFromAvatarMask(1, mask);
        
        var output = AnimationPlayableOutput.Create(m_Graph, "output", animator);
        output.SetSourcePlayable(m_AvatarMixer);
        m_Graph.Play();
    }



    void Update()
    {
        // m_MixerPlayable.SetInputWeight(0, (1.0f - weight));
        // m_MixerPlayable.SetInputWeight(1, (weight));
        m_AvatarMixer.SetInputWeight(1, weight);
    }

    void OnDisable()
    {
        m_Graph.Destroy();
    }
}