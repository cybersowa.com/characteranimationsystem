﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MechanimExampleController : MonoBehaviour
{
    public string WeightKey = "weight";
    public Animator Prefab;

    private float m_WeightOffset = 0.0f;
    private List<Animator> m_Instances;
    private int m_weightHash;

    private void Start()
    {
        var spawnPoints = Settings.GetSpawnPositions();

        m_Instances = new List<Animator>();
        m_weightHash = Animator.StringToHash(WeightKey);
        
        for (int i = 0; i < spawnPoints.Count; i++)
        {
            var animatorInstance = Instantiate<Animator>(Prefab, spawnPoints[i], Quaternion.identity, null);
            
            m_Instances.Add(
                animatorInstance
            );
        }
    }


    void Update()
    {
        m_WeightOffset = Settings.StepOffset(m_WeightOffset, Time.deltaTime);
            
        for (int i = 0; i < m_Instances.Count; i++)
        {
            m_Instances[i].SetFloat(m_weightHash, Settings.GetWeight(m_WeightOffset, i, m_Instances.Count));
        }
    }
}
