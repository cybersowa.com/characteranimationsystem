﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;


[RequireComponent(typeof(Animator))]
public class CharacterAnimator : MonoBehaviour
{
    public Vector2 Speed;
    [Header("Locomotion Anim Clips")]
    public AnimationClip Idle;
    public AnimationClip Forward;
    public AnimationClip Backward;
    public AnimationClip Left;
    public AnimationClip Right;

    public float LayerWeight = 0.0f;
    public AvatarMask Mask;
    public AnimationClip Sword;
    
    PlayableGraph m_Graph;
    private AnimationMixerPlayable m_LocomotionMixer;
    private AnimationMixerPlayable m_ForwardMixer;
    private AnimationMixerPlayable m_StrafeMixer;
    private AnimationLayerMixerPlayable m_MainMixer;
    
    void OnEnable()
    {
        var animator = GetComponent<Animator>();
        
        // Create graph with custom mixer.
        m_Graph = PlayableGraph.Create("AnimMixer");
        // m_Graph.SetTimeUpdateMode(DirectorUpdateMode.Manual);
        m_Graph.SetTimeUpdateMode(DirectorUpdateMode.GameTime);
        m_LocomotionMixer = AnimationMixerPlayable.Create(m_Graph, 2, false);
        m_ForwardMixer = AnimationMixerPlayable.Create(m_Graph, 3, false);
        m_StrafeMixer = AnimationMixerPlayable.Create(m_Graph, 3, false);
        
        
        m_ForwardMixer.ConnectInput(0, AnimationClipPlayable.Create(m_Graph, Forward), 0, 0.0f);
        m_ForwardMixer.ConnectInput(1, AnimationClipPlayable.Create(m_Graph, Idle), 0, 1.0f);
        m_ForwardMixer.ConnectInput(2, AnimationClipPlayable.Create(m_Graph, Backward), 0, 0.0f);
        
        
        m_StrafeMixer.ConnectInput(0, AnimationClipPlayable.Create(m_Graph, Left), 0, 0.0f);
        m_StrafeMixer.ConnectInput(1, AnimationClipPlayable.Create(m_Graph, Idle), 0, 1.0f);
        m_StrafeMixer.ConnectInput(2, AnimationClipPlayable.Create(m_Graph, Right), 0, 0.0f);
        
        
        m_LocomotionMixer.ConnectInput(0, m_ForwardMixer, 0, 1.0f);
        m_LocomotionMixer.ConnectInput(1, m_StrafeMixer, 0, 0.0f);
        
        m_MainMixer = AnimationLayerMixerPlayable.Create(m_Graph, 2);
        m_MainMixer.ConnectInput(0, m_LocomotionMixer, 0, 1.0f);
        m_MainMixer.ConnectInput(1, AnimationClipPlayable.Create(m_Graph, Sword), 0, LayerWeight);
        m_MainMixer.SetLayerMaskFromAvatarMask(1, Mask);
        // m_MainMixer.AddInput(AnimatorControllerPlayable.Create(m_Graph, animator.runtimeAnimatorController), 0, 0.0f);
        
        var output = AnimationPlayableOutput.Create(m_Graph, "output", animator);
        // var secondaryOutput = AnimationPlayableOutput.Create(m_Graph, "helper output", animator);
        output.SetSourcePlayable(m_MainMixer);
        // secondaryOutput.SetSourcePlayable(AnimatorControllerPlayable.Create(m_Graph, animator.runtimeAnimatorController));
        m_Graph.Play();
    }

    public float TimeScale = 0.0f;

    void Update()
    {
        m_MainMixer.SetInputWeight(1, LayerWeight);
        // m_MainMixer.SetTime(0.0f);
        SetLocomotionWeights(Speed);
        // m_Graph.Evaluate(Time.deltaTime * TimeScale);
    }

    void SetLocomotionWeights(Vector2 input)
    {
        input = Vector2.ClampMagnitude(input, 1.0f);
        
        float forward = Mathf.Clamp01(input.y);
        float backward = Mathf.Clamp01(-input.y);
        float left = Mathf.Clamp01(-input.x);
        float right = Mathf.Clamp01(input.x);

        float forwardIdle = (1.0f - (forward + backward));
        float strafeIdle = (1.0f - (right + left));
        
        m_ForwardMixer.SetInputWeight(0, forward);
        m_ForwardMixer.SetInputWeight(1, forwardIdle);
        m_ForwardMixer.SetInputWeight(2, backward);
        
        m_StrafeMixer.SetInputWeight(0, right);
        m_StrafeMixer.SetInputWeight(1, strafeIdle);
        m_StrafeMixer.SetInputWeight(2, left);

        float forwardMixer = (forward + backward);
        float strafeMixer = (1.0f - forwardMixer);
        m_LocomotionMixer.SetInputWeight(0, forwardMixer);
        m_LocomotionMixer.SetInputWeight(1, strafeMixer);
    }

    void OnDisable()
    {
        m_Graph.Destroy();
    }
}

/* Version 2 - working 2d blend
[RequireComponent(typeof(Animator))]
public class CharacterAnimator : MonoBehaviour
{
    public Vector2 Speed;
    [Header("Locomotion Anim Clips")]
    public AnimationClip Idle;
    public AnimationClip Forward;
    public AnimationClip Backward;
    public AnimationClip Left;
    public AnimationClip Right;

    enum AnimIndex
    {
        Idle = 0,
        Forward = 1,
        Backwards = 2,
        Left = 3,
        Right = 4
    }
    
    PlayableGraph m_Graph;
    private AnimationMixerPlayable m_AvatarMixer;
    private AnimationMixerPlayable m_ForwardMixer;
    private AnimationMixerPlayable m_StrafeMixer;
    
    void OnEnable()
    {
        var animator = GetComponent<Animator>();
        
        // Create graph with custom mixer.
        m_Graph = PlayableGraph.Create("AnimMixer");
        m_Graph.SetTimeUpdateMode(DirectorUpdateMode.GameTime);
        m_AvatarMixer = AnimationMixerPlayable.Create(m_Graph, 2, false);
        m_ForwardMixer = AnimationMixerPlayable.Create(m_Graph, 3, false);
        m_StrafeMixer = AnimationMixerPlayable.Create(m_Graph, 3, false);
        
        
        m_ForwardMixer.ConnectInput(0, AnimationClipPlayable.Create(m_Graph, Forward), 0, 0.0f);
        m_ForwardMixer.ConnectInput(1, AnimationClipPlayable.Create(m_Graph, Idle), 0, 1.0f);
        m_ForwardMixer.ConnectInput(2, AnimationClipPlayable.Create(m_Graph, Backward), 0, 0.0f);
        
        
        m_StrafeMixer.ConnectInput(0, AnimationClipPlayable.Create(m_Graph, Left), 0, 0.0f);
        m_StrafeMixer.ConnectInput(1, AnimationClipPlayable.Create(m_Graph, Idle), 0, 1.0f);
        m_StrafeMixer.ConnectInput(2, AnimationClipPlayable.Create(m_Graph, Right), 0, 0.0f);
        
        
        m_AvatarMixer.ConnectInput(0, m_ForwardMixer, 0, 1.0f);
        m_AvatarMixer.ConnectInput(1, m_StrafeMixer, 0, 0.0f);
        
        // m_AvatarMixer.ConnectInput((int)AnimIndex.Idle, AnimationClipPlayable.Create(m_Graph, Idle), 0, 1.0f);
        // m_AvatarMixer.ConnectInput((int)AnimIndex.Forward, AnimationClipPlayable.Create(m_Graph, Forward), 0, 0.0f);
        // m_AvatarMixer.ConnectInput((int)AnimIndex.Backwards, AnimationClipPlayable.Create(m_Graph, Backward), 0, 0.0f);
        // m_AvatarMixer.ConnectInput((int)AnimIndex.Left, AnimationClipPlayable.Create(m_Graph, Left), 0, 0.0f);
        // m_AvatarMixer.ConnectInput((int)AnimIndex.Right, AnimationClipPlayable.Create(m_Graph, Right), 0, 0.0f);
        
        var output = AnimationPlayableOutput.Create(m_Graph, "output", animator);
        output.SetSourcePlayable(m_AvatarMixer);
        m_Graph.Play();
    }



    void Update()
    {
        SetLocomotionWeights(Speed);
    }

    void SetLocomotionWeights(Vector2 input)
    {
        input = Vector2.ClampMagnitude(input, 1.0f);
        
        float forward = Mathf.Clamp01(input.y);
        float backward = Mathf.Clamp01(-input.y);
        float left = Mathf.Clamp01(-input.x);
        float right = Mathf.Clamp01(input.x);
        // float idle = Mathf.Clamp01(1.0f - input.magnitude);

        float forwardIdle = (1.0f - (forward + backward));
        float strafeIdle = (1.0f - (right + left));
        
        m_ForwardMixer.SetInputWeight(0, forward);
        m_ForwardMixer.SetInputWeight(1, forwardIdle);
        m_ForwardMixer.SetInputWeight(2, backward);
        
        m_StrafeMixer.SetInputWeight(0, right);
        m_StrafeMixer.SetInputWeight(1, strafeIdle);
        m_StrafeMixer.SetInputWeight(2, left);

        float forwardMixer = (forward + backward);
        float strafeMixer = (1.0f - forwardMixer);
        m_AvatarMixer.SetInputWeight(0, forwardMixer);
        m_AvatarMixer.SetInputWeight(1, strafeMixer);

        // m_AvatarMixer.SetInputWeight((int) AnimIndex.Idle, idle);
        // m_AvatarMixer.SetInputWeight((int) AnimIndex.Forward, forward);
        // m_AvatarMixer.SetInputWeight((int) AnimIndex.Backwards, backward);
        // m_AvatarMixer.SetInputWeight((int) AnimIndex.Left, left);
        // m_AvatarMixer.SetInputWeight((int) AnimIndex.Right, right);
    }

    void OnDisable()
    {
        m_Graph.Destroy();
    }
}
*/


/*
 Version 1
 
[RequireComponent(typeof(Animator))]
public class CharacterAnimator : MonoBehaviour
{
    public Vector2 Speed;
    [Header("Locomotion Anim Clips")]
    public AnimationClip Idle;
    public AnimationClip Forward;
    public AnimationClip Backward;
    public AnimationClip Left;
    public AnimationClip Right;

    enum AnimIndex
    {
        Idle = 0,
        Forward = 1,
        Backwards = 2,
        Left = 3,
        Right = 4
    }
    
    PlayableGraph m_Graph;
    private AnimationMixerPlayable m_AvatarMixer;
    
    void OnEnable()
    {
        var animator = GetComponent<Animator>();
        
        // Create graph with custom mixer.
        m_Graph = PlayableGraph.Create("AnimMixer");
        m_Graph.SetTimeUpdateMode(DirectorUpdateMode.GameTime);
        m_AvatarMixer = AnimationMixerPlayable.Create(m_Graph, 5, true);
        m_AvatarMixer.ConnectInput((int)AnimIndex.Idle, AnimationClipPlayable.Create(m_Graph, Idle), 0, 1.0f);
        m_AvatarMixer.ConnectInput((int)AnimIndex.Forward, AnimationClipPlayable.Create(m_Graph, Forward), 0, 0.0f);
        m_AvatarMixer.ConnectInput((int)AnimIndex.Backwards, AnimationClipPlayable.Create(m_Graph, Backward), 0, 0.0f);
        m_AvatarMixer.ConnectInput((int)AnimIndex.Left, AnimationClipPlayable.Create(m_Graph, Left), 0, 0.0f);
        m_AvatarMixer.ConnectInput((int)AnimIndex.Right, AnimationClipPlayable.Create(m_Graph, Right), 0, 0.0f);
        
        var output = AnimationPlayableOutput.Create(m_Graph, "output", animator);
        output.SetSourcePlayable(m_AvatarMixer);
        m_Graph.Play();
    }



    void Update()
    {
        SetLocomotionWeights(Speed);
    }

    void SetLocomotionWeights(Vector2 input)
    {
        input = Vector2.ClampMagnitude(input, 1.0f);
        
        float forward = Mathf.Clamp01(input.y);
        float backward = Mathf.Clamp01(-input.y);
        float left = Mathf.Clamp01(-input.x);
        float right = Mathf.Clamp01(input.x);
        float idle = Mathf.Clamp01(1.0f - input.magnitude);

        m_AvatarMixer.SetInputWeight((int) AnimIndex.Idle, idle);
        m_AvatarMixer.SetInputWeight((int) AnimIndex.Forward, forward);
        m_AvatarMixer.SetInputWeight((int) AnimIndex.Backwards, backward);
        m_AvatarMixer.SetInputWeight((int) AnimIndex.Left, left);
        m_AvatarMixer.SetInputWeight((int) AnimIndex.Right, right);
    }

    void OnDisable()
    {
        m_Graph.Destroy();
    }
}
*/
