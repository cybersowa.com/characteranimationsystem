﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CASReferenceAnimator : MonoBehaviour
{
    public Vector2 Speed;
    
    private Animator Animator;
    
    void Awake()
    {
        Animator = GetComponent<Animator>();
    }

    void Update()
    {
        Animator.SetFloat("Forward", Speed.y);
        Animator.SetFloat("Strafe", Speed.x);
    }
}
