﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicExampleController : MonoBehaviour
{
    public SimpleMixer Prefab;

    private float m_WeightOffset = 0.0f;
    private List<SimpleMixer> m_Instances;

    private void Start()
    {
        var spawnPoints = Settings.GetSpawnPositions();

        m_Instances = new List<SimpleMixer>();
        
        for (int i = 0; i < spawnPoints.Count; i++)
        {
            m_Instances.Add(
            Instantiate<SimpleMixer>(Prefab, spawnPoints[i], Quaternion.identity, null)
                );
        }
    }


    void Update()
    {
        m_WeightOffset = Settings.StepOffset(m_WeightOffset, Time.deltaTime);
            
        for (int i = 0; i < m_Instances.Count; i++)
        {
            m_Instances[i].weight = Settings.GetWeight(m_WeightOffset, i, m_Instances.Count);
        }
    }
}
