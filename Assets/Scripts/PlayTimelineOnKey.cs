﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class PlayTimelineOnKey : MonoBehaviour
{
    public List<Pair> Timelines = new List<Pair>();
    
    void Update()
    {
        for (int i = 0; i < Timelines.Count; i++)
        {
            if (Input.GetKeyDown(Timelines[i].Key))
            {
                Timelines[i].Timeline.Play();
            }   
        }
    }
    
    [System.Serializable]
    public struct Pair
    {
        public KeyCode Key;
        public PlayableDirector Timeline;
    }
}
