﻿using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    private const string AnimMixerExample = "AnimMixerExample";
    private const string BasicExample = "BasicExample";
    private const string MechanimExample = "MechanimExample";
    
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadScene(AnimMixerExample, LoadSceneMode.Single);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            SceneManager.LoadScene(BasicExample, LoadSceneMode.Single);
        }
        else if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            SceneManager.LoadScene(MechanimExample, LoadSceneMode.Single);
        }
        else if(Input.GetKeyDown(KeyCode.Q))
        {
            Application.Quit();
        }
    }
}
