﻿using UnityEditor.Timeline;
using UnityEngine;
using UnityEngine.Playables;

namespace CharacterAnimationSystem
{
    public class CASBehaviour : PlayableBehaviour
    {
        public AnimationClip Clip;
        public int Priority;
        public double Start;
        public double End;

        private double m_LastScrubingTime;
        private bool m_FirstFrame = false;

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            CASAnimator animator = playerData as CASAnimator;

            if (CASSystem.IsPreview == false)
            {
                if (m_FirstFrame)
                {
                    animator.RequestResetTime(new CASSystem.ResetTimeRequest()
                    {
                        Clip = Clip
                    });
                }
                
                animator.RequestPlay(new CASSystem.PlayRequest()
                {
                    Clip = Clip,
                    Priority = Priority
                });
            }
            else
            {
                // the instance is active, but not playing and the time has changed
                var playableDirector = TimelineEditor.inspectedDirector;
                var isDirectorActive = playableDirector.playableGraph.IsValid() &&
                                       !playableDirector.playableGraph.IsPlaying();
                
                if(isDirectorActive)
                {
                    double localTime = playableDirector.time - Start;
                    
                    var isScrub = m_LastScrubingTime != localTime;
                    if (isScrub)
                    {
                        animator.Initialize();
                        animator.Solver.SetUpateMode(DirectorUpdateMode.Manual);
                        CASSystem.EditorRegister(animator.Solver);

                        animator.RequestPlay(new CASSystem.PlayRequest()
                        {
                            Clip = Clip,
                            Priority = Priority
                        });

                        CASSystem.Update();

                        m_LastScrubingTime = localTime;
                        animator.Solver.MainMixer.SetTime(0.0f);
                        if (animator.Solver.GetIndexOf(Clip, out CASAnimIndex index))
                        {
                            animator.Solver.LayerMixers[index.Layer].GetInput(index.SocketIndex).SetTime(localTime);
                        }
                        
                        animator.Solver.Graph.Evaluate();
                    }
                }
            }

            m_FirstFrame = false;
        }

        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            m_FirstFrame = true;
        }
    }
}