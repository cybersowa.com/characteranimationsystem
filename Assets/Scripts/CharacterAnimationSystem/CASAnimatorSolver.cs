﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Assertions;
using UnityEngine.Playables;

namespace CharacterAnimationSystem
{
    public class CASAnimatorSolver
    {
        public PlayableGraph Graph;
        public AnimationLayerMixerPlayable MainMixer;
        //Locomotion Layer
        public AnimationMixerPlayable LocomotionMixer;
        public AnimationMixerPlayable ForwardMixer;
        public AnimationMixerPlayable StrafeMixer;
        //Other Layers
        public AnimationMixerPlayable[] LayerMixers;

        public bool IsInitialized { get; private set; } = false;
        public DirectorUpdateMode UpdateMode { get; private set; }

        public Animator Animator { get; private set; }
        public int LayerCount => LayerStates.Length;

        public Vector2 LocomotionState;
        public InputState[] LayerStates;
        public InputState[][] InputStates;

        public CASLayerConfig[] LayerConfigs;
        public CASClipConfig[][] ClipConfigs;

        private Dictionary<AnimationClip, CASAnimIndex> m_ClipIndexes = new Dictionary<AnimationClip, CASAnimIndex>();
        
        public void Initialize(Animator animator, CASController controller)
        {
            Assert.IsFalse(IsInitialized, "Don't initialize already initialized solver");

            Animator = animator;
            
            IsInitialized = true;
            
            int layerNumber = controller.Layers.Count;
            int mainMixerInputCount = layerNumber + 1;
            
            {//initialize dictionary of anim indexes
                //skip locomotion clips for dictionary

                for (int i = 0; i < controller.Layers.Count; i++)
                {
                    var layer = controller.Layers[i];
                    
                    for (int j = 0; j < layer.Clips.Count; j++)
                    {
                        var clip = layer.Clips[j].Clip;
                        var index = new CASAnimIndex()
                        {
                            Layer = i,
                            SocketIndex = j + 1//first socket is empty, for cinematic, hence + 1
                        };
                        
                        m_ClipIndexes.Add(clip, index);
                    }
                }
            }
            
            {//build anim graph
                const bool NormalizeWeightsByDefault = false;

                {//initialize graph itself
                    Graph = PlayableGraph.Create($"{animator.name}-{controller.name}");
                    SetUpateMode(DirectorUpdateMode.GameTime);
                }
                
                {//locomotion mixer
                    LocomotionMixer = AnimationMixerPlayable.Create(Graph, 2, NormalizeWeightsByDefault);
                    ForwardMixer = AnimationMixerPlayable.Create(Graph, 3, NormalizeWeightsByDefault);
                    StrafeMixer = AnimationMixerPlayable.Create(Graph, 3, NormalizeWeightsByDefault);

                    ForwardMixer.ConnectInput(0, AnimationClipPlayable.Create(Graph, controller.Forward), 0, 0.0f);
                    ForwardMixer.ConnectInput(1, AnimationClipPlayable.Create(Graph, controller.Idle), 0, 1.0f);
                    ForwardMixer.ConnectInput(2, AnimationClipPlayable.Create(Graph, controller.Backward), 0, 0.0f);

                    StrafeMixer.ConnectInput(0, AnimationClipPlayable.Create(Graph, controller.Left), 0, 0.0f);
                    StrafeMixer.ConnectInput(1, AnimationClipPlayable.Create(Graph, controller.Idle), 0, 1.0f);
                    StrafeMixer.ConnectInput(2, AnimationClipPlayable.Create(Graph, controller.Right), 0, 0.0f);

                    LocomotionMixer.ConnectInput(0, ForwardMixer, 0, 1.0f);
                    LocomotionMixer.ConnectInput(1, StrafeMixer, 0, 0.0f);
                }
                
                //create layer mixers
                LayerMixers = new AnimationMixerPlayable[layerNumber];
                for (int i = 0; i < LayerMixers.Length; i++)
                {
                    var layer = controller.Layers[i];
                    LayerMixers[i] = AnimationMixerPlayable.Create(Graph, layer.Count + 1, NormalizeWeightsByDefault);
                    LayerMixers[i].ConnectInput(0, AnimationClipPlayable.Create(Graph, null), 0);//cinematic anim input
                    
                    for (int j = 0; j < layer.Count; j++)
                    {
                        var clip = layer.Clips[j].Clip;
                        var index = m_ClipIndexes[clip];
                        LayerMixers[index.Layer].ConnectInput(index.SocketIndex, AnimationClipPlayable.Create(Graph, clip), 0);
                    }
                }

                {//final layered mixer
                    MainMixer = AnimationLayerMixerPlayable.Create(Graph, mainMixerInputCount);
                    
                    //set locomotion layer as the only active by default
                    MainMixer.ConnectInput(0, LocomotionMixer, 0, 1.0f);
                    for (int i = 0; i < controller.Layers.Count; i++)
                    {
                        var layer = controller.Layers[i];
                        MainMixer.ConnectInput(i + 1, LayerMixers[i], 0, 0.0f);
                        
                        if (layer.Mask != null)
                        {
                            MainMixer.SetLayerMaskFromAvatarMask((uint)i + 1, layer.Mask);
                        }

                        MainMixer.SetLayerAdditive((uint)i + 1, layer.IsAdditive);
                    }
                }

                {//connect outputs
                    var output = AnimationPlayableOutput.Create(Graph, "output", Animator);
                    output.SetSourcePlayable(MainMixer);
                }
            }

            {//initialize state lists
                LayerStates = new InputState[layerNumber];
                InputStates = new InputState[layerNumber][];

                for (int i = 0; i < layerNumber; i++)
                {
                    InputStates[i] = new InputState[controller.Layers[i].Count+1];//+1 coz we need a slot for cinematic anim
                }
            }

            {//copy configurations
                LayerConfigs = new CASLayerConfig[layerNumber];
                
                for (int i = 0; i < layerNumber; i++)
                {
                    LayerConfigs[i] = controller.Layers[i].Config;
                }
                
                ClipConfigs = new CASClipConfig[layerNumber][];//ignore locomotion layer
                
                for (int i = 0; i < layerNumber; i++)
                {
                    var layer = controller.Layers[i];
                    ClipConfigs[i] = new CASClipConfig[layer.Count + 1];//+1 to accomodate cinematic clip slot
                    ClipConfigs[i][0] = new CASClipConfig()//set default blend time for cinematic anim so we will avoid pop. Should be overriden by cinematic anim request
                    {
                        BlendInTime = CASSystem.DefaultBlendInTime
                    };
                    
                    for (int j = 0; j < layer.Count; j++)
                    {
                        ClipConfigs[i][j + 1] = layer.Clips[j].Config;
                    }
                }
            }
        }

        public void StartPlayingGraph()
        {
            Assert.IsTrue(IsInitialized, "Solver not initialized!");
            
            Graph.Play();
        }

        public void StopPlayingGraph()
        {
            Assert.IsTrue(IsInitialized, "Solver not initialized!");
            
            Graph.Stop();
        }

        public void Release()
        {
            Assert.IsTrue(IsInitialized, "Don't release uninitialized solver");
            
            Graph.Destroy();
            IsInitialized = false;
        }

        /// <summary>
        /// switch to update mode Manual for previewing animations in editor tools
        /// use GameTime in runtime so animations can be calculated in multithreaded jobs
        ///
        /// GameTime is default when initializing Solver
        /// </summary>
        public void SetUpateMode(DirectorUpdateMode updateMode)
        {
            Assert.IsTrue(IsInitialized, "Solver not initialized!");

            UpdateMode = updateMode;
            Graph.SetTimeUpdateMode(updateMode);
        }

        public bool GetIndexOf(AnimationClip clip, out CASAnimIndex index)
        {
            if (m_ClipIndexes.TryGetValue(clip, out index))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public struct InputState
        {
            /// <summary>
            /// Before normalization
            /// </summary>
            public float Weight;
        }
    }

    public struct CASAnimIndex
    {
        public int Layer;
        public int SocketIndex;
    }
}