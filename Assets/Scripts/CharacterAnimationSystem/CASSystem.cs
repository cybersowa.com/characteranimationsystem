﻿using System.Collections;
using System.Collections.Generic;
using CharacterAnimationSystem;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;

namespace CharacterAnimationSystem
{
    
    /// <summary>
    /// Locomotion layer is evaluated constantly
    /// Requests for override layers compete for highest priority
    /// Requests for additive layer(s) compete for highest priority
    /// There's always one layer active. We can go up to 3 layers active at one (4 if you include blending between overrides)
    ///
    /// TODO: implement support for cinematic anims. This will require updating anim graph whenever cinematic anim clip is changed which WILL SPIKE AND ALLOCATE.
    /// No way around that until Unity fixes it or we get access to Unity source code and spend couple of weeks/months
    /// </summary>
    public static class CASSystem
    {
        public const float DefaultBlendInTime = 0.1f;
        public const int InitialRequesCapacity = 5;
        public const float ErrorDelta = 0.001f; 
        
        private static List<CASAnimatorSolver> m_Animators = new List<CASAnimatorSolver>();
        private static Dictionary<CASAnimatorSolver, List<PlayRequest>> m_PlayRequests = new Dictionary<CASAnimatorSolver, List<PlayRequest>>();
        private static Dictionary<CASAnimatorSolver, List<ResetTimeRequest>> m_ResetTimeRequests = new Dictionary<CASAnimatorSolver, List<ResetTimeRequest>>();

        private static bool[] m_HasPlayRequest = new bool[0];
        private static CASAnimIndex[] m_RequestIndexes = new CASAnimIndex[0];
        private static PlayRequest[] m_FilteredRequests = new PlayRequest[0];

        /// <summary>
        /// TODO: use that to bypass blending while doing preview scrubbing
        /// </summary>
        public static bool IsPreview
        {
            get
            {
                #if UNITY_EDITOR
                return !Application.isPlaying;
                #else
                return false;
                #endif
            }
        }
        
        public static void Reset()
        {
            m_Animators.Clear();
            m_PlayRequests.Clear();
            m_ResetTimeRequests.Clear();
        }

        public static void Update()
        {
            float dTime = Time.deltaTime;
            
            for (int a = 0; a < m_Animators.Count; a++)
            {
                var animator = m_Animators[a];

                var resetRequests = m_ResetTimeRequests[animator];
                for (int j = 0; j < resetRequests.Count; j++)
                {
                    if (animator.GetIndexOf(resetRequests[j].Clip, out CASAnimIndex index))
                    {
                        ResetTime(animator, index);
                    }
                    else
                    {
                        Debug.LogWarning($"request for animation {resetRequests[j].Clip} not setup in controller");
                    }
                }
                resetRequests.Clear();
                
                //increase our cache if needed
                if (animator.LayerCount > m_HasPlayRequest.Length)
                {
                    const int ExtraReserve = 3;
                    int targetSize = animator.LayerCount + ExtraReserve;
                    m_HasPlayRequest = new bool[targetSize];//fields defaults to false | obvious but noting as important
                    m_RequestIndexes = new CASAnimIndex[targetSize];
                    m_FilteredRequests = new PlayRequest[targetSize];
                }

                var playRequests = m_PlayRequests[animator];
                for (int i = 0; i < playRequests.Count; i++)
                {
                    var request = playRequests[i];
                    if (animator.GetIndexOf(request.Clip, out CASAnimIndex index))
                    {
                        if (m_HasPlayRequest[index.Layer])
                        {
                            if (m_FilteredRequests[index.Layer].Priority <= request.Priority)
                            {
                                m_FilteredRequests[index.Layer] = request;
                                m_RequestIndexes[index.Layer] = index;
                            }
                        }
                        else
                        {
                            m_HasPlayRequest[index.Layer] = true;
                            m_FilteredRequests[index.Layer] = request;
                            m_RequestIndexes[index.Layer] = index;
                        }
                    }
                    else
                    {
                        Debug.LogWarning($"request for animation {request.Clip} not setup in controller");
                    }
                }

                for (int i = 0; i < animator.LayerCount; i++)
                {
                    float layerWeight = animator.LayerStates[i].Weight;
                    float blendTime = 0.0f;
                    float weightDelta = 0.0f;

                    if (m_HasPlayRequest[i])
                    {
                        blendTime = animator.LayerConfigs[i].BlendInTime;
                        weightDelta = (blendTime > ErrorDelta) ? dTime / blendTime : 1.0f;
                        layerWeight = Mathf.Clamp01(layerWeight + weightDelta);

                        var requestIndex = m_RequestIndexes[i];
                        blendTime = animator.ClipConfigs[i][requestIndex.SocketIndex].BlendInTime;
                        weightDelta = (blendTime > ErrorDelta) ? dTime / blendTime : 1.0f;
                        var clipStates = animator.InputStates[i];
                        for (int j = 0; j < clipStates.Length; j++)
                        {
                            float weight = clipStates[j].Weight;
                            if (j == requestIndex.SocketIndex)
                            {
                                weight += weightDelta;
                            }
                            else
                            {
                                weight -= weightDelta;
                            }

                            clipStates[j].Weight = Mathf.Clamp01(weight);
                        }
                    }
                    else
                    {
                        blendTime = animator.LayerConfigs[i].BlendOutTime;
                        weightDelta = (blendTime > ErrorDelta) ? dTime / blendTime : 1.0f;
                        layerWeight = Mathf.Clamp01(layerWeight - weightDelta);
                    }
                    
                    animator.LayerStates[i].Weight = layerWeight;
                }
                playRequests.Clear();
                
                //clear up caches
                for (int i = 0; i < m_HasPlayRequest.Length; i++)
                {
                    m_HasPlayRequest[i] = false;
                    m_FilteredRequests[i] = default;
                }
                
                UpdateWeights(animator);
            }
        }

        public static void Register(CASAnimatorSolver animator)
        {
            if (m_Animators.Contains(animator))
            {
                Debug.LogWarning($"{animator.Animator.name} already registered");
                return;
            }

            m_Animators.Add(animator);
            m_PlayRequests.Add(animator, new List<PlayRequest>(InitialRequesCapacity));
            m_ResetTimeRequests.Add(animator, new List<ResetTimeRequest>(InitialRequesCapacity));
        }

        public static void Unregister(CASAnimatorSolver animator)
        {
            if (m_Animators.Remove(animator) == false)
            {
                Debug.LogWarning($"{animator.Animator.name} wasn't registered");
            }

            m_PlayRequests.Remove(animator);
            m_ResetTimeRequests.Remove(animator);
        }
        
        public static void EditorRegister(CASAnimatorSolver animator)
        {
#if UNITY_EDITOR
            if (m_Animators.Contains(animator) == false)
            {
                Register(animator);
            }
#endif
        }

        public static void RequestPlay(CASAnimatorSolver animator, PlayRequest request)
        {
            if (m_PlayRequests.TryGetValue(animator, out List<PlayRequest> playRequests))
            {
                playRequests.Add(request);
            }
            else
            {
                Debug.LogWarning($"{animator.Animator.name} wasn't registered. Request ignored.");
            }
        }

        public static void RequestResetTime(CASAnimatorSolver animator, ResetTimeRequest request)
        {
            if (m_ResetTimeRequests.TryGetValue(animator, out List<ResetTimeRequest> playRequests))
            {
                playRequests.Add(request);
            }
            else
            {
                Debug.LogWarning($"{animator.Animator.name} wasn't registered. Request ignored.");
            }
        }
        

        private static void ResetTime(CASAnimatorSolver animator, CASAnimIndex index)
        {
            Playable input = animator.LayerMixers[index.Layer].GetInput(index.SocketIndex);
            input.SetTime(0.0);
        }

        private static void UpdateWeights(CASAnimatorSolver animator)
        {
            animator.MainMixer.SetInputWeight(0, 1.0f);

            for (int i = 0; i < animator.LayerStates.Length; i++)
            {
                int layerInput = i + 1;
                animator.MainMixer.SetInputWeight(layerInput, animator.LayerStates[i].Weight);
                
                float weightSum = 0.0f;
                for (int j = 0; j < animator.InputStates[i].Length; j++)
                {
                    weightSum += animator.InputStates[i][j].Weight;
                }

                if (weightSum > 0.0f)
                {
                    for (int j = 0; j < animator.InputStates[i].Length; j++)
                    {
                        float weight = animator.InputStates[i][j].Weight;
                        weight /= weightSum;
                        animator.LayerMixers[i].SetInputWeight(j, weight);
                    }
                }
            }

            {//update locomotion layer
                float mixerForwardWeight = Mathf.Clamp01(Mathf.Abs(animator.LocomotionState.y));
                animator.LocomotionMixer.SetInputWeight(0, mixerForwardWeight);
                animator.LocomotionMixer.SetInputWeight(1, (1.0f - mixerForwardWeight));

                float forwardWeight = Mathf.Clamp(animator.LocomotionState.y, -1.0f, 1.0f);
                float forwardFront = Mathf.Clamp01(forwardWeight);
                float forwardBack = Mathf.Clamp01(-forwardWeight);
                float forwardIdle = 1.0f - Mathf.Abs(forwardWeight);
                animator.ForwardMixer.SetInputWeight(0, forwardFront);
                animator.ForwardMixer.SetInputWeight(1, forwardIdle);
                animator.ForwardMixer.SetInputWeight(2, forwardBack);
                
                float strafeWeight = Mathf.Clamp(animator.LocomotionState.x, -1.0f, 1.0f);
                float strafeLeft = Mathf.Clamp01(-strafeWeight);
                float strafeRight = Mathf.Clamp01(strafeWeight);
                float strafeIdle = 1.0f - Mathf.Abs(strafeWeight);
                animator.StrafeMixer.SetInputWeight(0, strafeLeft);
                animator.StrafeMixer.SetInputWeight(1, strafeIdle);
                animator.StrafeMixer.SetInputWeight(2, strafeRight);
            }
        }
        
        public struct PlayRequest
        {
            public AnimationClip Clip;
            public int Priority;
        }
        
        public struct ResetTimeRequest
        {
            public AnimationClip Clip;
        }
    }
}
