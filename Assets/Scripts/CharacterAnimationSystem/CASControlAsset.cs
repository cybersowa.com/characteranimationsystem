﻿using UnityEngine;
using UnityEngine.Playables;

namespace CharacterAnimationSystem
{
    public class CASControlAsset : PlayableAsset
    {
        public AnimationClip Clip;
        public int Priority;
        public double Start;
        public double End;

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<CASBehaviour>.Create(graph);
            var casBehaviour = playable.GetBehaviour();
            casBehaviour.Clip = Clip;
            casBehaviour.Priority = Priority;
            casBehaviour.Start = Start;
            casBehaviour.End = End;

            return playable;
        }
    }
}