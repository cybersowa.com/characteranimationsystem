﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace CharacterAnimationSystem
{
    [TrackClipType(typeof(CASControlAsset))]
    [TrackBindingType((typeof(CASAnimator)))]
    public class CASTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            foreach (var clip in GetClips())
            {
                var asset = clip.asset as CASControlAsset;
                if (asset)
                {
                    asset.Start = clip.start;
                    asset.End = clip.end;
                }
            }

            return base.CreateTrackMixer(graph, go, inputCount);
        }
    }
}