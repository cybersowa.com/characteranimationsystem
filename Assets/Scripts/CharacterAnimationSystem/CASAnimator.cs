﻿using System;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace CharacterAnimationSystem
{
    [RequireComponent(typeof(Animator))]
    public class CASAnimator : MonoBehaviour
    {
        public CASController Controller;

        //for quick test only
        //public Vector2 DEBUG_Movement;
        
        public CASAnimatorSolver Solver { get; private set; }
        private Animator m_Animator;

        private bool m_IsInitialized = false;

        public void RequestPlay(CASSystem.PlayRequest request)
        {
            CASSystem.RequestPlay(Solver, request);
        }

        public void RequestResetTime(CASSystem.ResetTimeRequest request)
        {
            CASSystem.RequestResetTime(Solver, request);
        }

        /// <summary>
        /// y - forward/backward
        /// x - sideways
        ///
        /// we expect normalized movement input
        /// </summary>
        /// <param name="movement"></param>
        public void SetMovement(Vector2 movement)
        {
            Solver.LocomotionState = movement;
        }
        
        public void Initialize()
        {
            if(m_IsInitialized == false)
            {
                m_IsInitialized = true;
                Solver = new CASAnimatorSolver();
                m_Animator = GetComponent<Animator>();
                Solver.Initialize(m_Animator, Controller);
            }
        }

        public void Release()
        {
            if (m_IsInitialized)
            {
                m_IsInitialized = false;
                Solver.Release();
            }
        }
        
        private void Awake()
        {
            Initialize();
        }

        private void OnEnable()
        {
            CASSystem.Register(Solver);
            Solver.StartPlayingGraph();
        }

        private void OnDisable()
        {
            CASSystem.Unregister(Solver);
            Solver.StopPlayingGraph();
        }

        private void OnDestroy()
        {
            Release();
        }
        

        //for quick test only
        //private void Update()
        //{
        //    SetMovement(DEBUG_Movement);
        //}
    }
}