﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterAnimationSystem
{
    [CreateAssetMenu(fileName = "CASController", menuName = "CAS/Controller")]
    public class CASController : ScriptableObject
    {
        [Header("Locomotion Layer")] public AnimationClip Idle;
        public AnimationClip Forward;
        public AnimationClip Backward;
        public AnimationClip Left;
        public AnimationClip Right;

        public List<CASLayer> Layers = new List<CASLayer>();
    }

    [System.Serializable]
    public class CASClip
    {
        public CASClipConfig Config;
        public AnimationClip Clip;
    }

    [System.Serializable]
    public class CASLayer
    {
        public string Name;
        [Tooltip("If layer shouldn't use mask just leave this field empty")]
        public AvatarMask Mask;
        public bool IsAdditive;
        public CASLayerConfig Config;
        public List<CASClip> Clips = new List<CASClip>();

        public int Count => Clips.Count;
    }
    
    [System.Serializable]
    public struct CASLayerConfig
    {
        public float BlendInTime;
        public float BlendOutTime;
    }
    
    [System.Serializable]
    public struct CASClipConfig
    {
        public float BlendInTime;
    }
}