﻿using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace CharacterAnimationSystem
{
    [RequireComponent(typeof(Animator))]
    public class CASAnimatorOld : MonoBehaviour
    {
        public Vector2 Speed;
        [Header("Locomotion Anim Clips")]
        public AnimationClip Idle;
        public AnimationClip Forward;
        public AnimationClip Backward;
        public AnimationClip Left;
        public AnimationClip Right;

        public float LayerWeight = 0.0f;
        public AvatarMask Mask;
        public AnimationClip Sword;
        
        public PlayableGraph m_Graph;
        public AnimationMixerPlayable m_LocomotionMixer;
        public AnimationMixerPlayable m_ForwardMixer;
        public AnimationMixerPlayable m_StrafeMixer;
        public AnimationLayerMixerPlayable m_MainMixer;

        public bool UpperBodyDirty = false;
        
        private AnimationClip m_UpperBodyClip;

        private bool IsInitialized { get; set; } = false;

        void OnEnable()
        {
            Init();
        }

        public void Init()
        {
            if (IsInitialized == false)
            {
                var animator = GetComponent<Animator>();
        
                // Create graph with custom mixer.
                m_Graph = PlayableGraph.Create($"{name}-CASAnimator");
                if (Application.isPlaying)
                {
                    m_Graph.SetTimeUpdateMode(DirectorUpdateMode.GameTime);
                }
                else
                {
                    m_Graph.SetTimeUpdateMode(DirectorUpdateMode.Manual);
                }
                m_LocomotionMixer = AnimationMixerPlayable.Create(m_Graph, 2, false);
                m_ForwardMixer = AnimationMixerPlayable.Create(m_Graph, 3, false);
                m_StrafeMixer = AnimationMixerPlayable.Create(m_Graph, 3, false);
            
            
                m_ForwardMixer.ConnectInput(0, AnimationClipPlayable.Create(m_Graph, Forward), 0, 0.0f);
                m_ForwardMixer.ConnectInput(1, AnimationClipPlayable.Create(m_Graph, Idle), 0, 1.0f);
                m_ForwardMixer.ConnectInput(2, AnimationClipPlayable.Create(m_Graph, Backward), 0, 0.0f);
            
            
                m_StrafeMixer.ConnectInput(0, AnimationClipPlayable.Create(m_Graph, Left), 0, 0.0f);
                m_StrafeMixer.ConnectInput(1, AnimationClipPlayable.Create(m_Graph, Idle), 0, 1.0f);
                m_StrafeMixer.ConnectInput(2, AnimationClipPlayable.Create(m_Graph, Right), 0, 0.0f);
            
            
                m_LocomotionMixer.ConnectInput(0, m_ForwardMixer, 0, 1.0f);
                m_LocomotionMixer.ConnectInput(1, m_StrafeMixer, 0, 0.0f);
            
                m_MainMixer = AnimationLayerMixerPlayable.Create(m_Graph, 2);
                m_MainMixer.ConnectInput(0, m_LocomotionMixer, 0, 1.0f);
                m_MainMixer.ConnectInput(1, AnimationClipPlayable.Create(m_Graph, null), 0, LayerWeight);
                m_MainMixer.SetLayerMaskFromAvatarMask(1, Mask);
                // m_MainMixer.AddInput(AnimatorControllerPlayable.Create(m_Graph, animator.runtimeAnimatorController), 0, 0.0f);
            
                var output = AnimationPlayableOutput.Create(m_Graph, "output", animator);
                output.SetSourcePlayable(m_MainMixer);
                m_Graph.Play();

                IsInitialized = true;
            }
        }
        
        public void Release()
        {
            if (IsInitialized == true)
            {
                m_Graph.Destroy();
                IsInitialized = false;
            }
        }

        public void SetUpperBodyClip(AnimationClip animationClip)
        {
            Init();
            if (m_UpperBodyClip != animationClip)
            {
                // var input = (AnimationClipPlayable)m_MainMixer.GetInput(1);
                m_MainMixer.DisconnectInput(1);
                m_MainMixer.ConnectInput(1, AnimationClipPlayable.Create(m_Graph, animationClip), 0, 0.0f);
            
                m_UpperBodyClip = animationClip;
            }
        }

        public void SetUpperBodyTime(double time)
        {
            var input = m_MainMixer.GetInput(1);
            input.SetTime(time);
        }
        
        void Update()
        {
            // m_MainMixer.SetInputWeight(1, LayerWeight);
            // m_MainMixer.SetTime(0.0f);
            SetLocomotionWeights(Speed);

            if (UpperBodyDirty == false)
            {
                SetUpperBodyClip(null);
            }

            UpperBodyDirty = false;
        }

        void SetLocomotionWeights(Vector2 input)
        {
            input = Vector2.ClampMagnitude(input, 1.0f);
        
            float forward = Mathf.Clamp01(input.y);
            float backward = Mathf.Clamp01(-input.y);
            float left = Mathf.Clamp01(-input.x);
            float right = Mathf.Clamp01(input.x);

            float forwardIdle = (1.0f - (forward + backward));
            float strafeIdle = (1.0f - (right + left));
        
            m_ForwardMixer.SetInputWeight(0, forward);
            m_ForwardMixer.SetInputWeight(1, forwardIdle);
            m_ForwardMixer.SetInputWeight(2, backward);
        
            m_StrafeMixer.SetInputWeight(0, right);
            m_StrafeMixer.SetInputWeight(1, strafeIdle);
            m_StrafeMixer.SetInputWeight(2, left);

            float forwardMixer = (forward + backward);
            float strafeMixer = (1.0f - forwardMixer);
            m_LocomotionMixer.SetInputWeight(0, forwardMixer);
            m_LocomotionMixer.SetInputWeight(1, strafeMixer);
        }

        void OnDisable()
        {
            Release();
        }
    }
}