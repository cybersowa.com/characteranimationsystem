﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Settings
{
    public static readonly Vector3 InitPosition = new Vector3(-InstanceOffset * (InstanceColumns/2.0f), 0.0f, -InstanceOffset * (InstanceRows/2.0f));
    public const int InstanceColumns = 10;
    public const int InstanceRows = 10;
    public const float InstanceOffset = 3.0f;

    public static List<Vector3> GetSpawnPositions()
    {
        List<Vector3> positions = new List<Vector3>();
        
        for (int k = 0; k < Settings.InstanceColumns; k++)
        {
            for (int r = 0; r < Settings.InstanceRows; r++)
            {
                positions.Add(
                    new Vector3(
                        InitPosition.x + k * InstanceOffset,
                        0.0f,
                        InitPosition.z + r * InstanceOffset
                        )
                    );
            }
        }

        return positions;
    }

    public static float StepOffset(float offset, float dTime)
    {
        return Mathf.Repeat((offset + dTime * 0.1f), 1.0f);
    }

    public static float GetWeight(float offset, int index, int count)
    {
        if (count < 2)
        {
            return Mathf.Repeat(offset, 1.0f);
        }

        float t = index / (float) (count - 1);
        return Mathf.Repeat((t + offset), 1.0f);
    }
}
