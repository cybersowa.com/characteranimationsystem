﻿using System;
using System.Collections;
using System.Collections.Generic;
using CharacterAnimationSystem;
using UnityEngine;
using UnityEngine.LowLevel;
using UnityEngine.PlayerLoop;

/// <summary>
/// Helper class that allows initalize CAS system for example projects
/// it should be replaced with something that properly integrated with target project
/// </summary>
public static class InitializeCAS
{
    [RuntimeInitializeOnLoadMethod]
    private static void AppStart()
    {
        var defaultSystems = PlayerLoop.GetDefaultPlayerLoop();
        var customUpdate = new PlayerLoopSystem()
        {
            updateDelegate = CASSystem.Update,
            type = typeof(CASSystem)
        };

        if (AddSystemAfter<Update.DirectorUpdate>(ref defaultSystems, customUpdate))
        {
            Debug.Log("<color=green>Added CASSystem to engine update loop</color>");
        }
        else
        {
            Debug.LogError("<color=red>failed to add CASSystem to engine update loop</color>");
        }
        
        PlayerLoop.SetPlayerLoop(defaultSystems);
    }
    

    private static bool AddSystemAfter<T>(ref PlayerLoopSystem system, PlayerLoopSystem toAdd)
    {
        if (system.subSystemList == null)
        {
            return false;
        }

        int systemIndex = Array.FindIndex(system.subSystemList, loopSystem => loopSystem.type == typeof(T));
        if (systemIndex >= 0)
        {
            bool successfullyAdded = false;
            
            PlayerLoopSystem[] newSystemList = new PlayerLoopSystem[system.subSystemList.Length + 1];
            int offset = 0;
            for (int i = 0; i < system.subSystemList.Length; i++)
            {
                newSystemList[i + offset] = system.subSystemList[i];
                
                if (i == systemIndex)
                {
                    offset += 1;
                    newSystemList[i + offset] = toAdd;
                    successfullyAdded = true;
                }
            }

            if (successfullyAdded)
            {
                system.subSystemList = newSystemList;
                return true;
            }
        }

        for (int i = 0; i < system.subSystemList.Length; i++)
        {
            if (AddSystemAfter<T>(ref system.subSystemList[i], toAdd))
            {
                return true;
            }
        }

        return false;
    }
}
