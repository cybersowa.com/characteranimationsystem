﻿using System;
using UnityEngine.Profiling;

public struct ProfilerSample : IDisposable
{
	private bool m_isDisposed;

	public ProfilerSample(string sampleName) : this() {
		Profiler.BeginSample(sampleName);
	}
	
	public void Dispose() {
		if (!m_isDisposed) {
			Profiler.EndSample();
			m_isDisposed = true;
		}
	}
}